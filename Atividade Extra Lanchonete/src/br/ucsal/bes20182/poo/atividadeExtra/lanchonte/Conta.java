package br.ucsal.bes20182.poo.atividadeExtra.lanchonte;

public class Conta {
	private Double valorTotal;

	public void adicionarItem(Integer codigo, Integer qtd) {
		int cont = 0;
		do {
			for (Item item : Menu.getItens()) {
				if (item.getCodigo() == codigo) {
					if (item.getSituacao().equals(SituacaoItemEnum.DISPONIVEL)) {
						valorTotal += item.getValorUnitario();
					} else {
						System.out.println("O produto requisitadop n�o est� disponivel");
						break;
					}

				}
			}
			cont++;
		} while (cont < qtd);
	}

	public void removerItem(Integer codigo, Integer qtd) {
		int cont = 0;
		do {
			for (Item item : Menu.getItens()) {
				if (item.getCodigo() == codigo) {
					valorTotal -= item.getValorUnitario();
				}
			}
			cont++;
		} while (cont < qtd);
	}

	public Double fecharConta() {
		return valorTotal;
	}

}
