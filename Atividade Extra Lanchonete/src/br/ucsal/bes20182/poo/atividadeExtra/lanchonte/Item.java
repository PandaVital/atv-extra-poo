package br.ucsal.bes20182.poo.atividadeExtra.lanchonte;

public class Item {

	private Integer codigo;

	private String nome;

	private Double valorUnitario;

	private SituacaoItemEnum situacao;

//Ageitar a Construção de item com o Enum em Disponivel
	public Item(Integer codigo, String nome, Double valorUnitario) {
		this.codigo = codigo;
		this.nome = nome;
		this.valorUnitario = valorUnitario;
		this.situacao=SituacaoItemEnum.DISPONIVEL;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public SituacaoItemEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoItemEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Item [codigo=" + codigo + ", nome=" + nome + ", valorUnitario=" + valorUnitario + ", situacao="
				+ situacao + "]";
	}

}
