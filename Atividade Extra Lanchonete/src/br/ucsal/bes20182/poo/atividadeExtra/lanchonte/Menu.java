package br.ucsal.bes20182.poo.atividadeExtra.lanchonte;

import java.util.ArrayList;
import java.util.List;

public class Menu {

	private static List<Item> itens = new ArrayList<Item>();

	public static List<Item> getItens() {
		return itens;
	}

	public void adicionaItem(Integer codigo, String nome, Double valorUnitario) {
		Item item = new Item(codigo, nome, valorUnitario);
		if (item.getSituacao().equals(SituacaoItemEnum.DISPONIVEL)) {
			itens.add(item);
		} else {
			alterarSituaçãoItem(codigo, item.getSituacao());
			itens.add(item);
		}
	}

	private void alterarSituaçãoItem(Integer codigo, SituacaoItemEnum situacaoItemEnum) {
		for (Item item : itens) {
			if (item.getCodigo() == codigo) {
				if (item.getSituacao().equals(SituacaoItemEnum.DISPONIVEL)) {
					// se situação disponivel alterar para indisponivel
					item.setSituacao(SituacaoItemEnum.INDISPONIVEL);
				} else if (item.getSituacao().equals(SituacaoItemEnum.INDISPONIVEL)) {
					// se então situação indisponmivel alterar para disponivel
					item.setSituacao(SituacaoItemEnum.DISPONIVEL);
				}
			}
		}

	}


	public void removerItem(Integer codigo) {
		for (Item item2 : itens) {
			if (item2.getCodigo() == codigo) {
				itens.remove(item2);
			}
		}
	}

	public void listarItensDisponiveis() {
		for (Item item : itens) {
			System.out.println(item.toString());
		}
	}

	public Item obterItem(Integer codigo) {
		for (Item item2 : itens) {
			if (item2.getCodigo() == codigo) {
				return item2;
			}
		}
		return null;
	}

}
