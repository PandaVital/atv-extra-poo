package br.ucsal.bes20182.poo.atividadeExtra.sitemaBancario;

public class ContaCorrenteEspecial extends ContaCorrente {

	private Double limiteDeCredito;

	public ContaCorrenteEspecial(String nomeDoCorrentista, Integer numeroDaAgencia) {
		super(nomeDoCorrentista, numeroDaAgencia);
	}

	@Override
	public void Sacar(Double saque) throws SaldoInsuficienteException {
		if (saque > saldo + limiteDeCredito) {
			throw new SaldoInsuficienteException();
		} else {
			saldo -= saque;
		}
	}

	@Override
	public String toString() {
		return "ContaCorrenteEspecial [limiteDeCredito=" + limiteDeCredito + ", toString()=" + super.toString() + "]";
	}

}
