package br.ucsal.bes20182.poo.atividadeExtra.sitemaBancario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Banco {

	private static List<ContaCorrente> contas = new ArrayList<ContaCorrente>();

	public void listarContas() {
		Collections.sort(contas);
		// Lista Ordenada Por Nome do Correntista

		for (ContaCorrente contaCorrente : contas) {
			System.out.println("ContaCorrente [nomeDoCorrentista=" + contaCorrente.getNomeDoCorrentista()
					+ ", numeroDaConta=" + contaCorrente.getNumeroDaConta() + ", saldo=" + contaCorrente.getSaldo());
		}

	}

	public void listarContasAgencia() {
		Collections.sort(contas, new Comparator<ContaCorrente>() {

			@Override
			public int compare(ContaCorrente conta1, ContaCorrente conta2) {
				int resultado = conta1.getNumeroDaAgencia().compareTo(conta2.getNumeroDaAgencia());
				if (resultado == 0) {
					resultado = conta1.getNumeroDaConta().compareTo(conta2.getNumeroDaConta());
				}
				return resultado;
			}
		});
		for (ContaCorrente contaCorrente : contas) {
			System.out.println("ContaCorrente [nomeDoCorrentista=" + contaCorrente.getNomeDoCorrentista()
					+ ", numeroDaConta=" + contaCorrente.getNumeroDaConta() + ", numeroDaAgencia="
					+ contaCorrente.getNumeroDaAgencia());
		}
	}

	public void realizarSaque(Integer numeroDaConta, Double saque) {
		for (ContaCorrente contaCorrente : contas) {
			if (contaCorrente.getNumeroDaConta() == numeroDaConta) {
				try {
					contaCorrente.Sacar(saque);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				System.out.println("Seu Saldo Atual � = " + contaCorrente.getSaldo());
			}
		}

	}
}
