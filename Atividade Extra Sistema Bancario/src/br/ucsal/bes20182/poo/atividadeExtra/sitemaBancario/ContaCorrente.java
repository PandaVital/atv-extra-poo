package br.ucsal.bes20182.poo.atividadeExtra.sitemaBancario;

public class ContaCorrente implements Comparable<ContaCorrente> {
	private String nomeDoCorrentista;

	private Integer numeroDaAgencia;

	protected Double saldo;

	private Integer numeroDaConta;

	private static Integer cont = 1;

	public ContaCorrente(String nomeDoCorrentista, Integer numeroDaAgencia) {
		super();
		this.nomeDoCorrentista = nomeDoCorrentista;
		this.numeroDaAgencia = numeroDaAgencia;
		numeroDaConta = cont;
		cont++;
	}

	public void Sacar(Double saque) throws SaldoInsuficienteException {
		if (saque > saldo) {
			throw new SaldoInsuficienteException();
		} else {
			saldo -= saque;
		}
	}

	public void depositar(Integer deposito) {
		saldo += deposito;
	}

	public Double consultarSaldo() {
		return saldo;
	}

	public String getNomeDoCorrentista() {
		return nomeDoCorrentista;
	}

	public void setNomeDoCorrentista(String nomeDoCorrentista) {
		this.nomeDoCorrentista = nomeDoCorrentista;
	}

	public Integer getNumeroDaAgencia() {
		return numeroDaAgencia;
	}

	public Double getSaldo() {
		return saldo;
	}

	public Integer getNumeroDaConta() {
		return numeroDaConta;
	}

	public static Integer getCont() {
		return cont;
	}

	@Override
	public String toString() {
		return "ContaCorrente [nomeDoCorrentista=" + nomeDoCorrentista + ", numeroDaAgencia=" + numeroDaAgencia
				+ ", saldo=" + saldo + ", numeroDaConta=" + numeroDaConta + "]";
	}

	@Override
	public int compareTo(ContaCorrente conta) {
		return nomeDoCorrentista.compareTo(conta.getNomeDoCorrentista());
	}

}
