package br.ucsal.bes20182.poo.atividadeExtra.sitemaBancario;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Diversos {
	public static Scanner sc = new Scanner(System.in);

	public void obterListaItens() {
		int cont = 0;

		String nome;

		List<String> itens = new ArrayList<String>();

		do {
			System.out.println("Ja foram informados " + cont + "nomes de itens");
			System.out.println("Informe o Nome de Um Item");
			nome = sc.nextLine();
			itens.add(nome);
			cont++;

		} while (cont < 100);

	}

	public void obterListaMembros() {

		String[] nomes = new String[10];

		String nome;
		
		for (int i = 0; i < nomes.length; i++) {
			System.out.println("Informe o nome do participante da equipe");
			nome = sc.nextLine();
			if (i == 0) {
				nomes[i] = nome;
			} else if (i > 0) {
				for (int j = i; j > -1; j--) {// Esse for percorre o Arrey de nomes de forma decrescete

					if (nomes[j] == nome) { // Verificando se o nome informado ja esta no arrey , se sim o descarta
						nome = null;
						i--;    /* Aqui Decrementa 1 de i pois quando chegar no final e for acrescentado 1 ele
								 * ficara na mesma posi��o at� um nome valido ser escrito
								 */

					} else {// Caso contrario o adiciona na procima posi�ao livre
						nomes[i] = nome;
					}
				}
			}
		}
	}
}